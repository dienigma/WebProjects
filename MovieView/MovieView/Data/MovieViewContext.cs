﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MovieView.Models
{
    public class MovieViewContext : DbContext
    {
        public MovieViewContext (DbContextOptions<MovieViewContext> options)
            : base(options)
        {
        }

        public DbSet<MovieView.Models.Movie> Movie { get; set; }
    }
}
