﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieView.Models
{
    public class Movie
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Release { get; set; }
        public string Plot { get; set; }
        public string Cast { get; set; }
        public string Producer { get; set; }
    }
}
