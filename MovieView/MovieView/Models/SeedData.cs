﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace MovieView.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MovieViewContext(
                serviceProvider.GetRequiredService<DbContextOptions<MovieViewContext>>()))
            {
                // Look for any movies.
                if (context.Movie.Any())
                {
                    return;   // DB has been seeded
                }

                context.Movie.AddRange(
                    new Movie
                    {
                        Title = "Split",
                        Release = 2016,
                        Plot = "Kevin, who is suffering from dissociative identity disorder and has 23 alter egos, kidnaps three teenagers. They must figure out his friendly personas before he unleashes his 24th personality.",
                        Cast = "James McAvoy, Anya Taylor-Joy",
                        Producer = "Jason Blum"

                    },
                    new Movie
                    {
                        Title = "Rush",
                        Release = 2013,
                        Plot = "James Hunt and Niki Lauda, two extremely skilled Formula One racers, have an intense rivalry with each other. However, it is their enmity that pushes them to their limits.",
                        Cast = "Chris Hemsworth, Daniel Brühl, Olivia Wilde",
                        Producer = "Ron Howard"

                    }


                );
                context.SaveChanges();
            }
        }
    }
}