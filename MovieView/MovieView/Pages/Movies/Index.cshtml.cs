﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MovieView.Models;

namespace MovieView.Pages.Movies
{
    public class IndexModel : PageModel
    {
        private readonly MovieView.Models.MovieViewContext _context;

        public IndexModel(MovieView.Models.MovieViewContext context)
        {
            _context = context;
        }

        public IList<Movie> Movie { get;set; }

        public async Task OnGetAsync()
        {
            Movie = await _context.Movie.ToListAsync();
        }
    }
}
